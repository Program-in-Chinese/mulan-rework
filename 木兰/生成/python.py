from __future__ import absolute_import
from astunparse import Unparser
from six.moves import cStringIO
import ast
import sys

函数转换 = {
    "println": "print",
    "tuple": "",
    "char": "chr",
    "isa": "isinstance",
    "ceil": "math.ceil",
    "floor": "math.floor",
    "fabs": "math.fabs",
    "sqrt": "math.sqrt",
    "log": "math.log",
    "log10": "math.log10",
    "exp": "math.exp",
    "pow": "math.pow",
    "sin": "math.sin",
    "cos": "math.cos",
    "tan": "math.tan",
    "asin": "math.asin",
    "acos": "math.acos",
}

class 转(Unparser):
    def __init__(self, tree, file = sys.stdout):
        self.f = file
        self.future_imports = []
        self._indent = 0
        self.dispatch(tree)
        self.v = cStringIO()
        self.f.flush()
        self.v.write("\nimport sys\nfrom math import *\nARGV = sys.argv[1:]" + self.f.getvalue())
        self.v.flush()

    def _Name(self, 节点):
        if 节点.id == "PI":
            self.write("pi")
        else:
            self.write(节点.id)
            
    def _Call(self, 节点):
        if isinstance(节点.func, ast.Name):
            if 节点.func.id in 函数转换:
                节点.func.id = 函数转换[节点.func.id]
        super()._Call(节点)
        
def 转python(树):
    v = cStringIO()
    return 转(树, file=v).v.getvalue()